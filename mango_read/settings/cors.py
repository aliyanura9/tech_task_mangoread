from decouple  import config, Csv

CORS_ORIGIN_ALLOW_ALL = True

CORS_ORIGIN_WHITELIST = config('CORS_ALLOWED_ORIGINS', cast=Csv())

CORS_ALLOW_METHODS = [
    'DELETE',
    'GET',
    'OPTIONS',
    'PATCH',
    'POST',
    'PUT',
]

CORS_ALLOW_HEADERS = [
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'accept-language',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
]
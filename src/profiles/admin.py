from django.contrib import admin
from django.contrib.auth.models import Group
from rest_framework.authtoken.models import TokenProxy
from src.profiles.models import User

admin.site.unregister(Group)
admin.site.unregister(TokenProxy)

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'is_staff', 'is_active', 'is_superuser', 'is_deleted')
    list_display_links = ('username',)
    list_filter = ('username', 'is_staff', 'is_active', 'is_superuser', 'is_deleted')
    fields = ('username', 'is_superuser', 'is_staff', 'is_active', 'is_deleted',
              'created_at', 'updated_at')
    readonly_fields = ('created_at', 'updated_at') 
from src.profiles.models import User
from rest_framework import serializers


class RegistrationSerializer(serializers.ModelSerializer):
    '''User login serializer
    required fields: username, password
    fields: avatar, username, nickname, password'''

    class Meta:
        model = User
        fields = ('avatar', 'username', 'nickname', 'password')


class LoginSerializer(serializers.Serializer):
    '''User login serializer
    required fields: username, password'''

    username = serializers.CharField(min_length=2, required=True)
    password = serializers.CharField(min_length=2, required=True)


class UserSerializer(serializers.ModelSerializer):
    '''User details serializer
    fields: id, avatar, username, nickname'''

    class Meta:
        model = User
        fields = ('id', 'avatar', 'username', 'nickname')


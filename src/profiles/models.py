from django.contrib.auth.models import (
        AbstractUser, PermissionsMixin
    )
from django.utils.translation import gettext_lazy as _
from django.db import models
from src.profiles.managers import CustomUserManager


class User(AbstractUser, PermissionsMixin):
    avatar = models.ImageField(max_length=500, blank=True, null=True,
                               upload_to='user/avatar/%Y/%m/%d',
                               verbose_name=_('аватар'),)
    username = models.CharField(
        max_length=150, unique=True,
        verbose_name=_('имя пользователя'), blank=False, null=False,
        error_messages={
            'unique': _("Такой пользователь уже существует."),
        },
    )
    nickname = models.CharField(max_length=150, verbose_name=_('прозвище'),
                                blank=False, null=False)
    is_superuser = models.BooleanField(default=False, blank=True,
                                       verbose_name=_('администратор'))
    is_staff = models.BooleanField(default=False, blank=True,
                                   verbose_name=_('сотрудник системы'))
    is_active = models.BooleanField(default=False, blank=True,
                                    verbose_name=_('активный'))
    is_deleted = models.BooleanField(default=False, blank=False,
                                     verbose_name=_('удален'))
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('дата создания'))
    updated_at = models.DateTimeField(auto_now=True,
                                      verbose_name=_('дата обновления'))

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.username

    class Meta:
        db_table = 'usr'
        verbose_name = _('Пользователь')
        verbose_name_plural = _('Пользователи')
        ordering = ('created_at',)
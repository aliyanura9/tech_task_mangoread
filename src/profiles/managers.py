from django.contrib.auth.models import BaseUserManager
from django.utils import timezone


class CustomUserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, username, password, is_active, is_superuser,
                     is_staff, is_deleted, **extra_fields):
        """
            user creation
        """
        now = timezone.now()
        user = self.model(
            username=username,
            is_active=is_active,
            is_superuser=is_superuser,
            is_staff=is_staff,
            is_deleted=is_deleted,
            last_login=now,
            created_at=now,
            **extra_fields
        )
        if password:
            user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password=None, **extra_fields):
        """
            user creation
        """
        return self._create_user(username, password, True, False,
                                 False, False, **extra_fields)

    def create_superuser(self, username, password=None, **extra_fields):
        """
            superuser creation
        """
        user = self._create_user(username, password, True, True,
                                 True, False, **extra_fields)
        user.save(using=self._db)
        return user
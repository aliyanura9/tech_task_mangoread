from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from src.profiles.serializers import RegistrationSerializer,\
                                     LoginSerializer, UserSerializer
from src.profiles.services import UserService, TokenService


class RegistrationViewSet(ModelViewSet):
    """User registration view"""

    serializer_class = RegistrationSerializer
    permission_classes = (AllowAny,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = UserService.create_user(
            avatar=serializer.validated_data.get('avatar'),
            username=serializer.validated_data.get('username'),
            nickname=serializer.validated_data.get('nickname'),
            password=serializer.validated_data.get('password'),
        )
        return Response(data={
            'message': 'The user has successfully registered and the profile has been successfully created',
            'status': 'CREATED'
        }, status=status.HTTP_201_CREATED)


class LoginViewSet(ModelViewSet):
    """User login view"""

    serializer_class = LoginSerializer
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user, token = TokenService.create_auth_token(
            username=serializer.validated_data.get('username'),
            password=serializer.validated_data.get('password')
        )
        return Response(data={
            'message': 'You have successfully logged in',
            'data': {
                'token': str(token),
                'token_type': 'Token',
                'user_id': user.pk,
            },
            'status': "OK"
        }, status=status.HTTP_200_OK)


class UserViewSet(ModelViewSet):
    """User details view"""

    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    def retrieve(self, request, *args, **kwargs):
        instance = request.user
        serializer = self.get_serializer(instance)
        return Response(serializer.data)
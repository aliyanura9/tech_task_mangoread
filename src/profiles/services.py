from typing import Tuple
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from src.profiles.models import User
from src.profiles.exceptions import ObjectNotFoundException,\
                                    ValidationException


class TokenService:
    '''Token service with "create_auth_token" function to authenticate a user and get or create him a token'''
    
    model = Token

    @classmethod
    def create_auth_token(cls, username: str, password: str) -> Tuple[User, Token, Token]:
        user = authenticate(username=username, password=password)
        if user:
            token, created = cls.model.objects.get_or_create(user=user)
            return user, token
        else:
            raise ObjectNotFoundException('User not found or not active')


class UserService:
    '''User service with "create_user" function to create a new user'''

    model = User

    @classmethod
    def create_user(cls, avatar:str, username: str,
                    nickname: str, password: str) -> User:
        return cls.model.objects.create_user(
            username=username,
            password=password,
            avatar=avatar,
            nickname=nickname
        )
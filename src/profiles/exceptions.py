from rest_framework.exceptions import APIException


class ObjectNotFoundException(APIException):
    status_code = 404


class ValidationException(APIException):
    status_code = 400
from django.urls import path
from src.profiles.views import RegistrationViewSet,\
                               LoginViewSet, UserViewSet


urlpatterns = [
    path('', UserViewSet.as_view({"get": 'retrieve'}), name='details'),
    path('registration', RegistrationViewSet.as_view({"post": 'create'}), name='reqistration'),
    path('login', LoginViewSet.as_view({"post": 'post'}), name='login'),
]

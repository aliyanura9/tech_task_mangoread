from typing import List
from src.review.models import Review
from src.profiles.models import User
from src.manga.services import MangaService


class ReviewService:
    '''Review service with "create" function to create a new review
    and "filter" function with dynamic field indication'''

    model = Review

    @classmethod
    def filter(cls, *args, **kwargs) -> List[model]:
        return cls.model.objects.filter(*args, **kwargs)

    @classmethod
    def create(cls, user: User, manga_id: int,
                    content: str) -> Review:
        manga = MangaService.get(id=manga_id)
        return cls.model.objects.create(
            user=user,
            manga=manga,
            content=content
        )
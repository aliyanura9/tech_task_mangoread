from django.db import models
from django.utils.translation import gettext_lazy as _
from src.profiles.models import User
from src.manga.models import Manga


class Review(models.Model):
    user = models.ForeignKey(User, related_name='reviews',
                             on_delete=models.CASCADE,
                             verbose_name=_('Пользователь'))
    manga = models.ForeignKey(Manga, related_name='reviews',
                              on_delete=models.CASCADE,
                              verbose_name=_('Манга'))
    content = models.TextField(verbose_name=_('Содержание'))
    created_at = models.DateField(auto_now=True, verbose_name=_('Дата создания'))

    class Meta:
        db_table = 'reviews'
        verbose_name = _('Рецензия')
        verbose_name_plural = _('Рецензии')
        ordering = ('-created_at',)

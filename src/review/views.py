from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ModelViewSet
from src.review.serializers import ReviewSerializer,\
                                   ReviewCreateSerializer
from src.review.models import Review
from src.review.services import ReviewService
from src.review.pagination import ReviewPagination


class ReviewViewSet(ModelViewSet):
    """Manga types list view"""

    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = (AllowAny,)
    pagination_class = ReviewPagination

    def get_serializer(self, *args, **kwargs):
        if self.request.method == 'GET':
            self.serializer_class = ReviewSerializer
        elif self.request.method == 'POST':
            self.serializer_class = ReviewCreateSerializer
        return super().get_serializer(*args, **kwargs)

    def get_queryset(self):
        queryset = ReviewService.filter(manga__id=self.kwargs.get('manga_pk'))
        return queryset

    def retrieve(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        ReviewService.create(
            user=request.user,
            manga_id=self.kwargs.get('manga_pk'),
            content=serializer.validated_data.get('content')
        )
        return Response(data={
            'message': 'The review has successfully created',
            'status': 'CREATED'
        }, status=status.HTTP_201_CREATED)
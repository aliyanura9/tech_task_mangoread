from django.contrib import admin
from src.review.models import Review

@admin.register(Review)
class StratumAdmin(admin.ModelAdmin):
    list_display = ('manga', 'user', 'created_at')
    list_display_links = ('manga',)
    ordering = ('manga', 'user')
    list_filter = ('user', 'manga', 'created_at')
    fields = ('user', 'manga', 'content', 'created_at')
    readonly_fields = ('created_at',)
    search_fields = ('content',)
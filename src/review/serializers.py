from rest_framework import serializers
from src.review.models import Review
from src.profiles.serializers import UserSerializer


class ReviewSerializer(serializers.ModelSerializer):
    '''Review list serializer
    fields: id, user, content'''

    user = UserSerializer(many=False)

    class Meta:
        model = Review
        fields = ('id', 'user', 'content')


class ReviewCreateSerializer(serializers.ModelSerializer):
    '''Review create serializer
    required fields: content'''

    class Meta:
        model = Review
        fields = ('content',)

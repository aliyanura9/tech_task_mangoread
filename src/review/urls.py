from django.urls import path
from src.review.views import ReviewViewSet


urlpatterns = [
    path('<int:manga_pk>', ReviewViewSet.as_view({"get": 'list', 'post': 'retrieve'}),
         name='review-list-retriewe'),
]
from django_filters.rest_framework import FilterSet, MultipleChoiceFilter,\
                                          ModelChoiceFilter,\
                                          BaseInFilter, NumberFilter
from src.manga.models import Manga, MangaType, Genre


class MangaFilter(FilterSet):
    '''Manga filtering by year of publication'''

    pub_year_min = NumberFilter(field_name='pub_year', lookup_expr='gte')
    pub_year_max = NumberFilter(field_name='pub_year', lookup_expr='lte')
    manga_type = BaseInFilter(
        field_name='manga_types__id',
        lookup_expr='in',
        distinct=True
    )
    genres = BaseInFilter(
        field_name='genres__id',
        lookup_expr='in',
        distinct=True
    )

    class Meta:
        model = Manga
        fields = ['pub_year_min', 'pub_year_max', 'manga_type', 'genres']
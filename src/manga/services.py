from src.manga.models import Manga
from src.profiles.exceptions import ObjectNotFoundException

class MangaService:
    '''Manga service with "get" function'''

    model = Manga

    @classmethod
    def get(cls, *args, **kwargs) -> model:
        try:
            return cls.model.objects.get(*args, **kwargs)
        except cls.model.DoesNotExist:
            raise ObjectNotFoundException(f'{cls.model.__name__} not found')
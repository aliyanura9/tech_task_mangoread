from rest_framework import serializers
from src.manga.models import MangaType, Genre, Manga


class MangaTypeListSerializer(serializers.ModelSerializer):
    '''Manga type serializer for list view
    fields: id, title'''

    class Meta:
        model = MangaType
        fields = ('id', 'title')


class GenreListSerializer(serializers.ModelSerializer):
    '''Genre serializer for list view
    fields: id, title'''

    class Meta:
        model = Genre
        fields = ('id', 'title')


class MangaListSerializer(serializers.ModelSerializer):
    '''Manga serializer for list view
    fields: id, title, cover, pub_year'''

    class Meta:
        model = Manga
        fields = ('id', 'title', 'cover', 'pub_year')

class MangaSerializer(serializers.ModelSerializer):
    '''Manga serializer for list view
    fields: id, title, cover, pub_year'''
    genres = GenreListSerializer(many=True)
    manga_type = MangaTypeListSerializer(many=False)

    class Meta:
        model = Manga
        fields = ('id', 'title', 'cover', 'pub_year',
                  'info', 'manga_type', 'genres', 'synopsis')

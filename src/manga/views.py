from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter
from django_filters.rest_framework import DjangoFilterBackend
from src.manga.filters import MangaFilter
from src.manga.serializers import MangaTypeListSerializer, MangaSerializer,\
                                  GenreListSerializer, MangaListSerializer
from src.manga.models import MangaType, Genre, Manga


class MangaTypeViewSet(ModelViewSet):
    """Manga types list view"""

    queryset = MangaType.objects.all()
    serializer_class = MangaTypeListSerializer
    permission_classes = (AllowAny,)
    pagination_class = None


class GenreViewSet(ModelViewSet):
    """Manga genres list view"""

    queryset = Genre.objects.all()
    serializer_class = GenreListSerializer
    permission_classes = (AllowAny,)
    pagination_class = None


class MangaViewSet(ModelViewSet):
    """Manga list view"""
    queryset = Manga.objects.all()
    serializer_class = MangaListSerializer
    permission_classes = (AllowAny,)
    filter_backends = (SearchFilter, DjangoFilterBackend)
    filterset_class = MangaFilter
    search_fields = ('title',)


class MangaDetailsViewSet(ModelViewSet):
    """Manga details view"""

    serializer_class = MangaSerializer
    permission_classes = (AllowAny,)
    lookup_field = 'pk'

    def get_queryset(self):
        mangas = Manga.objects.filter()
        return mangas

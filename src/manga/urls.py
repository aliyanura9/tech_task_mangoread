from django.urls import path
from src.manga.views import MangaTypeViewSet,\
                            GenreViewSet, MangaViewSet,\
                            MangaDetailsViewSet


urlpatterns = [
    path('', MangaViewSet.as_view({"get": 'list'}), name='mangas'),
    path('<int:pk>', MangaDetailsViewSet.as_view({"get": 'retrieve'}), name='mangas'),
    path('types', MangaTypeViewSet.as_view({"get": 'list'}), name='types'),
    path('genres', GenreViewSet.as_view({"get": 'list'}), name='genres'),
]
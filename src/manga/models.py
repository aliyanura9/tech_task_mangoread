from django.db import models
from django.utils.translation import gettext_lazy as _


class MangaType(models.Model):
    title = models.CharField(max_length=250, verbose_name=_('Наименование'))

    def __str__(self) -> str:
        return self.title

    class Meta:
        db_table = 'types'
        verbose_name = _('Тип')
        verbose_name_plural = _('Типы')
        ordering = ('title',)


class Genre(models.Model):
    title = models.CharField(max_length=250, verbose_name=_('Наименование'))

    def __str__(self) -> str:
        return self.title

    class Meta:
        db_table = 'genres'
        verbose_name = _('Жанр')
        verbose_name_plural = _('Жанры')
        ordering = ('title',)


class Manga(models.Model):
    cover = models.ImageField(max_length=500, blank=True, null=True,
                               upload_to='manga/covers/%Y/%m/%d',
                               verbose_name=_('Обложка'),)
    title = models.CharField(max_length=250, verbose_name=('Наименование'))
    info = models.CharField(max_length=250, verbose_name=_('Информация'))
    pub_year = models.SmallIntegerField(verbose_name=_('Год'))
    synopsis = models.TextField(verbose_name=_('Синопсис'))
    manga_type = models.ForeignKey(MangaType, on_delete=models.SET_NULL,
                                   null=True, blank=True,
                                   verbose_name=_('Тип'),
                                   related_name='mangas')
    genres = models.ManyToManyField(Genre, verbose_name=_('Жанры'),
                                    related_name='mangas')

    def __str__(self) -> str:
        return self.title
    
    class Meta:
        db_table = 'mangas'
        verbose_name = _('Манга')
        verbose_name_plural = _('Манги')
        ordering = ('title', 'pub_year')

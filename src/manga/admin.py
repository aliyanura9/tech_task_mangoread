from django.contrib import admin
from src.manga.models import MangaType, Genre, Manga


@admin.register(MangaType)
class StratumAdmin(admin.ModelAdmin):
    list_display = ('title',)
    list_display_links = ('title',)
    ordering = ('title',)
    list_filter = ('title',)
    fields = ('title',)


@admin.register(Genre)
class StratumAdmin(admin.ModelAdmin):
    list_display = ('title',)
    list_display_links = ('title',)
    ordering = ('title',)
    list_filter = ('title',)
    fields = ('title',)


@admin.register(Manga)
class StratumAdmin(admin.ModelAdmin):
    list_display = ('title', 'pub_year', 'manga_type')
    list_display_links = ('title',)
    ordering = ('title',)
    list_filter = ('title', 'pub_year', 'manga_type', 'genres')
    fields = ('cover', 'title', 'info', 'pub_year', 'manga_type',
              'genres', 'synopsis')
